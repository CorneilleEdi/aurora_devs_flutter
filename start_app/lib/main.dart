import 'package:flutter/material.dart';
import 'package:start_app/constants.dart';

void main() {
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        home: FirstPage());
  }
}

class FirstPage extends StatefulWidget {
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  int count = 0;

  @override
  void initState() {
    super.initState();
    count = 2;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("First page")),
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Count:",
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 12),
            Text(
              count.toString(),
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 12),
            RaisedButton(
              color: green,
              onPressed: () {
                setState(() {
                  count++;
                });
              },
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  "Click me",
                  style: TextStyle(fontSize: 22, color: Colors.white),
                ),
              ),
            ),
            SizedBox(height: 12),
            RaisedButton(
              color: Colors.white,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => SecondPage(),
                    ));
              },
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  "Go to  second page",
                  style: TextStyle(fontSize: 22, color: Colors.black54),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Second page")),
      body: Center(
        child: RaisedButton(
              color: Colors.white,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  "Go back",
                  style: TextStyle(fontSize: 22, color: Colors.black54),
                ),
              ),
            ),
      ),
    );
  }
}
