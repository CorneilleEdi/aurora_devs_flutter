import 'package:flutter/material.dart';

const Color gray = Color(0xff4A5067);
const Color gray_dark = Color(0xff32344A);
const Color green = Color(0xff00DD7C);