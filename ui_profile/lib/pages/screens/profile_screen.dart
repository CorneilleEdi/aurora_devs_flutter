import 'package:flutter/material.dart';
import 'package:ui_profile/pages/widgets/circular_border.dart';
import 'package:ui_profile/utils/colors_helper.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 6,
            child: Container(
              width: double.infinity,
              color: ColorsHelper.purple_dark,
              padding: const EdgeInsets.symmetric(vertical: 28, horizontal: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 90,
                    width: 90,
                    child: CircleAvatar(
                      backgroundColor: ColorsHelper.purple_light,
                      child: Text(
                        "A",
                        style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.bold,
                          color: ColorsHelper.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Text(
                    "Maidi Huff",
                    style: TextStyle(
                      fontSize: 22,
                      color: ColorsHelper.white,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "maidi_huff@gmail.com",
                    style: TextStyle(
                      fontSize: 16,
                      color: ColorsHelper.white,
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(50.0),
                    ),
                    color: ColorsHelper.purple_light,
                    onPressed: () {},
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12.0),
                      child: Text(
                        "Manage you account",
                        style: TextStyle(
                          color: ColorsHelper.white,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 8,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 22, horizontal: 22),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Container(
                      height: 50,
                      width: 50,
                      child: CircleAvatar(
                        backgroundColor: ColorsHelper.orange,
                        child: Text(
                          "M",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    title: Text(
                      "Maud Huff",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      "maud0hull@gmail.com",
                      style: TextStyle(
                        color: Colors.black87,
                      ),
                    ),
                  ),
                  ListTile(
                    leading: Container(
                      height: 50,
                      width: 50,
                      child: CircleAvatar(
                        backgroundColor: ColorsHelper.green,
                        child: Text(
                          "H",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    title: Text(
                      "Maud Huff",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      "huff_10@gmail.com",
                      style: TextStyle(
                        color: Colors.black87,
                      ),
                    ),
                    trailing: Container(
                      width: 20,
                      child: CircleAvatar(
                        backgroundColor: Colors.red,
                        child: Text(
                          "3",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                  ListTile(
                    leading: Container(
                      height: 50,
                      width: 50,
                      child: CircleAvatar(
                        backgroundColor: ColorsHelper.rose,
                        child: Text(
                          "H",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    title: Text(
                      "Maud Huff",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      "maudh@yahoo.com",
                      style: TextStyle(
                        color: Colors.black87,
                      ),
                    ),
                  ),
                  ListTile(
                    leading: Padding(
                      padding: const EdgeInsets.only(left: 4.0, top: 8),
                      child: CircularBorder(
                        width: 4,
                        size: 42,
                        color: Colors.grey,
                        icon: Icon(Icons.add, color: Colors.grey),
                      ),
                    ),
                    title: Text(
                      "Add another account",
                      style: TextStyle(
                        color: Colors.black54,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: Colors.grey[300]),
                ),
              ),
              child: Center(
                child: Text(
                  "Sign out of all accounts",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: ColorsHelper.purple_light,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
