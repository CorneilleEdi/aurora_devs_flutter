import 'package:flutter/material.dart';

class ColorsHelper {
  static const Color purple_dark = Color(0xff8130E8);
  static const Color purple_light = Color(0xffA372FF);
  static const Color rose = Color(0xffff158A);
  static const Color green = Color(0xff00CAAE);
  static const Color orange = Color(0xffff9142);
  static const Color white = Colors.white70;
}
