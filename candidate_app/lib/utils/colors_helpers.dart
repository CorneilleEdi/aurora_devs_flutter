import 'package:flutter/material.dart';

class ColorsHelper {
  static const Color ColorGreen = Color(0xff3FA142);
  static const Color ColorBlue = Color(0xff28B6CD);
  static const Color ColorOrange = Color(0xffFD8C00);
  static const Color ColorYellow = Color(0xffFFB400);
}