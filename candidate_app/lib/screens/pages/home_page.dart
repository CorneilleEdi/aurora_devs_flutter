import 'package:candidate_app/screens/widgets/colored_card.dart';
import 'package:candidate_app/screens/widgets/list_row.dart';
import 'package:candidate_app/utils/colors_helpers.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black54,
        leading: Icon(Icons.menu),
        title: Text("Candidates"),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(6.0),
            child: Row(
              children: <Widget>[
                ColoredCard(
                  background: ColorsHelper.ColorYellow,
                  title: "Unread",
                  number: 32,
                  icon: Icons.mail,
                ),
                SizedBox(width: 6.0),
                ColoredCard(
                  background: ColorsHelper.ColorOrange,
                  title: "Unrated",
                  number: 5,
                  icon: Icons.stars,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 6.0),
            child: Row(
              children: <Widget>[
                ColoredCard(
                  background: ColorsHelper.ColorBlue,
                  title: "Recently viewed",
                  number: 334,
                  icon: Icons.access_time,
                ),
                SizedBox(width: 6.0),
                ColoredCard(
                  background: ColorsHelper.ColorGreen,
                  title: "Ignored",
                  number: 0,
                  icon: Icons.account_box,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 4.0, right: 4.0, top: 8.0),
            child: Card(
              elevation: 4,
              child: Container(
                color: Colors.white,
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text(
                        "Overview",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Divider(
                      height: 12,
                    ),
                    ListRow(title: "Unscheduled", number: 32),
                    Divider(
                      height: 12,
                    ),
                    ListRow(title: "Slipping away", number: 5),
                    Divider(
                      height: 12,
                    ),
                    ListRow(title: "Popular", number: 15),
                    Divider(
                      height: 12,
                    ),
                    ListRow(title: "Pending", number: 0),
                    Divider(
                      height: 12,
                    ),
                    ListRow(title: "All candidates", number: 422)
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
