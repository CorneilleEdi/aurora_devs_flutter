import 'package:flutter/material.dart';

class ColoredCard extends StatelessWidget {
  final Color background;
  final String title;
  final int number;
  final IconData icon;

  ColoredCard({this.background, this.title, this.number, this.icon});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(3.0),
      ),
      elevation: 6,
      margin: const EdgeInsets.all(0),
      child: Container(
        decoration: BoxDecoration(
            color: background, borderRadius: BorderRadius.circular(3)),
        height: 100,
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 12, right: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 12,
              ),
              Text(
                title,
                style: TextStyle(color: Colors.white, fontSize: 22),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '$number Candidates',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  Icon(
                    icon,
                    color: Colors.white,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
